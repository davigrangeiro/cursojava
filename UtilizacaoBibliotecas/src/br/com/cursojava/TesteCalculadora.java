package br.com.cursojava;

public class TesteCalculadora {
	
	
	public static Integer multiply(Calculadora calc, Integer... numeros) {
		return calc.multiply(numeros);
	}
	
	public static Integer add(Calculadora calc, Integer... numeros) {
		return calc.add(numeros);
	}
	
	public static Integer minus(Calculadora calc, Integer... numeros) {
		return calc.minus(numeros);
	}
	
	public static Integer divide(Calculadora calc, Integer... numeros) {
		return calc.divide(numeros);
	}
	
	public static Integer fib(CalculadoraComplexa calc, Integer pos) {
		return calc.fib(pos);
	}
	
	public static void main(String[] args) {
		Calculadora c = new CalculadoraImpl();
		CalculadoraComplexa cc = new CalculadoraComplexaImpl();
		
		System.out.println(multiply(c, 1,2,2,3,4,5,6));
		System.out.println(add(c, 1,2,2,3,4,5,6));
		System.out.println(divide(c, 10000,2,2,3,4,5,6));
		System.out.println(minus(c, 520,2,2,3,4,5,6));
		System.out.println(fib(cc, 120));
	}
	
	
}
