package br.com.cursojava.designpatterns.composite;

public class Gerente extends Aprovador {

	public Gerente(String nome) {
		super(nome);
	}

	@Override
	public Double getLimiteEmprestimo() {
		return 25000.00;
	}

}
