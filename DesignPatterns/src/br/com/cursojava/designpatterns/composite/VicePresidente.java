package br.com.cursojava.designpatterns.composite;

public class VicePresidente extends Aprovador {

	public VicePresidente(String nome) {
		super(nome);
	}

	@Override
	public Double getLimiteEmprestimo() {
		return 500000.00;
	}

}
