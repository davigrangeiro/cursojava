package br.com.cursojava.designpatterns.composite;

public class Emprestimo {

	private Double valor;

	public Emprestimo(Double valor) {
		super();
		this.valor = valor;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
}
