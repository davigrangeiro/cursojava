package br.com.cursojava.designpatterns.composite;

public class Caixa extends Aprovador {

	public Caixa(String nome) {
		super(nome);
	}

	@Override
	public Double getLimiteEmprestimo() {
		return 5000.00;
	}

}
