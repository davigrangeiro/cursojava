package br.com.cursojava.designpatterns.composite;

public class TesteComposite {

	public static void main(String[] args) {
		Aprovador caixa      = new Caixa("João"),
				  gerente    = new Gerente("Joaquim"),
				  diretor    = new Diretor("Marcos"),
				  vp         = new VicePresidente("Maria"),
				  presidente = new Presidente("Sergio");
		
		caixa.setChefe(gerente);
		gerente.setChefe(diretor);
		diretor.setChefe(vp);
		vp.setChefe(presidente);
		
		expandirComposite(presidente);
		expandirComposite(vp);
		expandirComposite(diretor);
		expandirComposite(gerente);
		expandirComposite(caixa);
		
	}
	
	
	public static void expandirComposite(Aprovador aprovador) {
		String s = "Aprovador: " + aprovador.getNome() + "\n "
				 + "Tipo: " + aprovador.getClass() + "\n "
				 + "Valor Aprovação: " + aprovador.getLimiteEmprestimo();
		
		System.out.println(s);
		
		if (null != aprovador.getChefe()) {
			expandirComposite(aprovador.getChefe());
		} else {
			System.out.println("O aprovador " + aprovador.getNome() + " não tem chefe.");
		}
	}
	
	
}
