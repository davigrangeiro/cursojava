package br.com.cursojava.designpatterns.composite;

public class Diretor extends Aprovador {

	public Diretor(String nome) {
		super(nome);
	}

	@Override
	public Double getLimiteEmprestimo() {
		return 150000.00;
	}

}
