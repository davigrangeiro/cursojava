package br.com.cursojava.designpatterns.composite;

public class TesteChain {

	public static void main(String[] args) {
		Aprovador caixa      = new Caixa("João"),
				  gerente    = new Gerente("Joaquim"),
				  diretor    = new Diretor("Marcos"),
				  vp         = new VicePresidente("Maria"),
				  presidente = new Presidente("Sergio");
		
		caixa.setChefe(gerente);
		gerente.setChefe(diretor);
		diretor.setChefe(vp);
		vp.setChefe(presidente);
		
		Emprestimo e1 = new Emprestimo(100.00);
		Emprestimo e2 = new Emprestimo(1000.00);
		Emprestimo e3 = new Emprestimo(10000.00);
		Emprestimo e4 = new Emprestimo(100000000.00);
		Emprestimo e5 = new Emprestimo(10000000000000000000.00);
		
		try {
			caixa.aprovarEmprestimo(e1);
		} catch (EmprestimoNaoAprovadoException e) {
			e.printStackTrace();
		}
		try {
			caixa.aprovarEmprestimo(e2);
		} catch (EmprestimoNaoAprovadoException e) {
			e.printStackTrace();
		}
		try {
			caixa.aprovarEmprestimo(e3);
		} catch (EmprestimoNaoAprovadoException e) {
			e.printStackTrace();
		}
		try {
			caixa.aprovarEmprestimo(e4);
		} catch (EmprestimoNaoAprovadoException e) {
			e.printStackTrace();
		}
		try {
			caixa.aprovarEmprestimo(e5);
		} catch (EmprestimoNaoAprovadoException e) {
			e.printStackTrace();
		}
		
		
	}
	
	
}
