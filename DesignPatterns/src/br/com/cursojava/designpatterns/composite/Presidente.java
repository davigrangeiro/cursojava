package br.com.cursojava.designpatterns.composite;

public class Presidente extends Aprovador {

	public Presidente(String nome) {
		super(nome);
	}

	@Override
	public Double getLimiteEmprestimo() {
		return 500000000.00;
	}

}
