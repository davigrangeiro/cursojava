package br.com.cursojava.designpatterns.composite;

public abstract class Aprovador {

	private String nome;
	private Aprovador chefe;
	
	
	public Aprovador(String nome) {
		super();
		this.nome = nome;
	}

	public boolean aprovarEmprestimo (Emprestimo e) throws EmprestimoNaoAprovadoException {
		boolean res = true;
		
		if (e.getValor().compareTo(this.getLimiteEmprestimo()) <= 0) {
			aprovar(e);
		} else if (null != getChefe()) {
			res = getChefe().aprovarEmprestimo(e);
		} else {
			throw new EmprestimoNaoAprovadoException();
		}
 		
		return res;
	}

	
	private void aprovar(Emprestimo e) {
		String s = "Requisição aprovada >>>>>>>>>>>>>>> \n "
				 + "Aprovador: " + getNome() + "\n "
				 + "Tipo: " + this.getClass() + "\n "
				 + "Valor emprestimo: " + e.getValor();
		
		System.out.println(s);
	}

	public abstract Double getLimiteEmprestimo();


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public Aprovador getChefe() {
		return chefe;
	}


	public void setChefe(Aprovador chefe) {
		this.chefe = chefe;
	}
	
	
	
}
