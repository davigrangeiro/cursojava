package br.com.cursojava.designpatterns.decorator;

public class Power implements Calculus {

	@Override
	public Number doCalculus(Number number, Number number2) {
		Number res = new Number(number.value);
		for (int i = 1; i < number2.value.intValue(); i++) {
			res.value = res.value * number.value;
		}
		return res;
	}

}
