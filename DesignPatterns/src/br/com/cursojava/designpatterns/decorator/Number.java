package br.com.cursojava.designpatterns.decorator;

import java.util.LinkedList;
import java.util.List;

public class Number {

	public Double value = 0.0;
	
	private List<Calculus> calculusDecorator;
	
	public Number(Double value) {
		super();
		calculusDecorator = new LinkedList<>();
		this.value = value;
	}

	public Number() {
		super();
		calculusDecorator = new LinkedList<>();
	}
	
	public void register(Calculus c) {
		calculusDecorator.add(c);
	}
	
	public void cleanDecorator() {
		calculusDecorator.clear();
	}
	
	public void doCalculus(Number n) {
		for (Calculus calculus : calculusDecorator) {
			this.value = calculus.doCalculus(this, n).value;
		}
	}
	
	
	
}
