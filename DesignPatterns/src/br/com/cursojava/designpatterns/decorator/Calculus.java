package br.com.cursojava.designpatterns.decorator;

public interface Calculus {

	Number doCalculus(Number number, Number number2);

}
