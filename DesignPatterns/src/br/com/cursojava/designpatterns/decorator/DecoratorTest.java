package br.com.cursojava.designpatterns.decorator;

public class DecoratorTest {

	public static void main(String[] args) {
		Number n1 = new Number(5.6),
			   n2 = new Number(4.2),
		       n3 = new Number(3.0);
		
		Calculus[] c1 = {new Sum(), new Division(), new Division(), new Sum()},
				   c2 = {new Sum(), new Division(), new Division(), new Sum(), new Sum(), new Division(), new Division(), new Sum()},
				   c3 = {new Sum(), new Division(), new Division(), new Sum(), new Power(), new Power()};
		
		
		for (int i = 0; i < c1.length; i++) {
			n1.register(c1[i]);
		}
		
		for (int i = 0; i < c2.length; i++) {
			n2.register(c2[i]);
		}
		
		for (int i = 0; i < c3.length; i++) {
			n3.register(c3[i]);
		}
		
		n1.doCalculus(new Number(5.3));
		n2.doCalculus(new Number(9.6));
		n3.doCalculus(new Number(6.0));
		
		System.out.println(n1.value);
		System.out.println(n2.value);
		System.out.println(n3.value);
		
		
		
		
		
	}
	
}
