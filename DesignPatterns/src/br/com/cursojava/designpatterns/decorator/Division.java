package br.com.cursojava.designpatterns.decorator;

public class Division implements Calculus {

	@Override
	public Number doCalculus(Number number, Number number2) {
		return new Number(number.value / number2.value);
	}

}
