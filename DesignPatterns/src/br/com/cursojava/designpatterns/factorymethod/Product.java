package br.com.cursojava.designpatterns.factorymethod;

public interface Product {

	public Integer getValor();
	
}
