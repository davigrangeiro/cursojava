package br.com.cursojava.designpatterns.factorymethod;

public class ProductFamilyB implements Product {

	@Override
	public Integer getValor() {
		return 3;
	}

}
