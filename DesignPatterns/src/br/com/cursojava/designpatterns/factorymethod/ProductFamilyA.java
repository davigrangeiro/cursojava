package br.com.cursojava.designpatterns.factorymethod;

public class ProductFamilyA implements Product {

	@Override
	public Integer getValor() {
		return 2;
	}

}
