package br.com.cursojava.designpatterns.factorymethod;

public class FactoryMethodTest {

	public static void main(String[] args) {
		
		String a = null;
		if ("1".equals(a)) {
			testFamilyA();
		} else {
			testFamilyB();
		}
		
		if (a.equals("1")) {
			testFamilyA();
		} else {
			testFamilyB();
		}
	}

	private static void testFamilyA() {
		FactoryMethod factory;
		Product p;
		
		factory = new FactoryMethodFamilyA();
		
 		p = factory.create();
 		System.out.println(p.getValor());
	}


	private static void testFamilyB() {
		FactoryMethod factory;
		Product p;
		
		factory = new FactoryMethodFamilyB();
		
 		p = factory.create();
 		System.out.println(p.getValor());
	}
	
}
