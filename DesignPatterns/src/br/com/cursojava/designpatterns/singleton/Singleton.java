package br.com.cursojava.designpatterns.singleton;

public class Singleton {

	private static Singleton instance;
	
	public static Singleton getInstance() {
		if (null == instance) {
			instance = new Singleton();
		}
		return instance;
	}

	private Singleton() {
		super();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	public void print() {
		System.out.println("Terminei a criação, já estou apto a ser utilizado");
	}
	
	
	
	
}
