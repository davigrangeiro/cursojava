package br.com.cursojava.designpatterns.abstractfactory;

import br.com.cursojava.designpatterns.abstractfactory.exception.ProcessException;

public abstract class Process {
	
	public abstract void doProcess(String record) throws ProcessException;
	
}
