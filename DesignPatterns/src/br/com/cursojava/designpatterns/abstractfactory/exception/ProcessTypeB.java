package br.com.cursojava.designpatterns.abstractfactory.exception;

import br.com.cursojava.designpatterns.abstractfactory.Process;

public class ProcessTypeB extends Process {

	@Override
	public void doProcess(String record) throws ProcessException {
		if (record.charAt(4) != '2') {
			throw new ProcessException();
		}
		System.out.println(record);
	}

}
