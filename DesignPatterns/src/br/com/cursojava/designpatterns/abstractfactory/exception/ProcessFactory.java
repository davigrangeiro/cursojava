package br.com.cursojava.designpatterns.abstractfactory.exception;

import java.util.HashMap;
import java.util.Map;

import br.com.cursojava.designpatterns.abstractfactory.Process;

public class ProcessFactory {

	private static Map<Character, Process> processesMap;
	
	static {
		processesMap = new HashMap<>();
		
		processesMap.put('A', new ProcessTypeA());
		processesMap.put('B', new ProcessTypeB());
		processesMap.put('C', new ProcessTypeC());
		processesMap.put('D', new ProcessTypeD());
	}
	
	public static Process getProcessInstance(String record) throws ProcessException {
		Character first = record.charAt(0);
		
		if (processesMap.containsKey(first)) {
			return processesMap.get(first);
		} else {
			throw new ProcessException();
		}
		
	}
	
}
