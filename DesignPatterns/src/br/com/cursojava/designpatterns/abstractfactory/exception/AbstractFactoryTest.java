package br.com.cursojava.designpatterns.abstractfactory.exception;

public class AbstractFactoryTest {

	public static void main(String[] args) {
		String[] lines = {"AB561644",
						  "AB5616564",
						  "B5672446",
						  "B567344",
						  "AB567644",
						  "CAB537644",
						  "CAB567644000",
						  "D89962221",
						  "D89942221",
						  "Z89942221"		
			};
		
		
		processLines(lines);
	}

	private static void processLines(String[] lines) {
		for (String string : lines) {
			try {
				ProcessFactory.getProcessInstance(string).doProcess(string);
			} catch (ProcessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}
