package br.com.cursojava.designpatterns.abstractfactory.exception;

import br.com.cursojava.designpatterns.abstractfactory.Process;

public class ProcessTypeD extends Process {

	@Override
	public void doProcess(String record) throws ProcessException {
		// do stuff
		if (record.charAt(4) != '4') {
			throw new ProcessException();
		}
		System.out.println(record);
	}

}
