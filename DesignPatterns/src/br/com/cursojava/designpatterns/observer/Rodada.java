package br.com.cursojava.designpatterns.observer;

import java.util.ArrayList;
import java.util.List;

public class Rodada {

	private List<Jogo> jogos;

	public Rodada() {
		super();
		jogos = new ArrayList<Jogo>();
	}

	public List<Jogo> getJogos() {
		return jogos;
	}

	public void setJogos(List<Jogo> jogos) {
		this.jogos = jogos;
	}
	
}
