package br.com.cursojava.designpatterns.observer;

public class TesteTabelaClassificacaoDinamica {

	public static void main(String[] args) {
		Time saoPaulo = new Time("São Paulo"),
			 curintia = new Time("Corinthians"),
			 aguaSanta = new Time("Agua Santa"),
			 redBull = new Time("Red Bull"),
			 santos = new Time("Santos"),
			 palmeiras = new Time("Palmeiras");
		
		TabelaCampeonato tabela = new TabelaCampeonato();
		
		Rodada r = new Rodada();
		r.getJogos().add(new Jogo(saoPaulo, curintia, tabela));
		r.getJogos().add(new Jogo(aguaSanta, redBull, tabela));
		r.getJogos().add(new Jogo(santos, palmeiras, tabela));
		tabela.getRodadas().add(r);
		
		
		Rodada r2 = new Rodada();
		r2.getJogos().add(new Jogo(saoPaulo, aguaSanta, tabela));
		r2.getJogos().add(new Jogo(curintia, palmeiras, tabela));
		r2.getJogos().add(new Jogo(santos, redBull, tabela));
		tabela.getRodadas().add(r2);
		
		Rodada r3 = new Rodada();
		r3.getJogos().add(new Jogo(palmeiras, aguaSanta, tabela));
		r3.getJogos().add(new Jogo(curintia, redBull, tabela));
		r3.getJogos().add(new Jogo(santos, saoPaulo, tabela));
		tabela.getRodadas().add(r3);
		
		for (Rodada rodada : tabela.getRodadas()) {
			for (Jogo jogo : rodada.getJogos()) {
				jogo.jogar();
			}
		}
		
	}
	
	
}
