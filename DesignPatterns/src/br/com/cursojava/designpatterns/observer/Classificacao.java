package br.com.cursojava.designpatterns.observer;

import java.util.ArrayList;
import java.util.List;

public class Classificacao implements Comparable<Classificacao>{

	private Time time;
	
	private List<Jogo> jogos;
	
	public Integer getVitorias() {
		Integer res = 0;
		for (Jogo jogo : jogos) {
			if (this.time.equals(jogo.getTimeCasa()) && jogo.getPlacarTimeCasa().compareTo(jogo.getPlacarTimeVisitante()) > 0) {
				res+= 1;
				continue;
			}
			else if (jogo.getPlacarTimeVisitante().compareTo(jogo.getPlacarTimeCasa()) > 0) {
				res+= 1;
				continue;
			}
		}
		return res;
	}
	
	public Integer getEmpates() {
		Integer res = 0;
		for (Jogo jogo : jogos) {
			if (jogo.getPlacarTimeCasa().compareTo(jogo.getPlacarTimeVisitante()) == 0) {
				res+= 1;
			}
		}
		return res;
	}
	
	public Integer getDerrotas() {
		Integer res = 0;
		for (Jogo jogo : jogos) {
			if (this.time.equals(jogo.getTimeCasa()) && jogo.getPlacarTimeCasa().compareTo(jogo.getPlacarTimeVisitante()) < 0) {
				res+= 1;
				continue;
			}
			else if (jogo.getPlacarTimeVisitante().compareTo(jogo.getPlacarTimeCasa()) < 0) {
				res+= 1;
				continue;
			}
		}
		return res;
	}
	
	public Integer getPontos() {
		return (3 * this.getVitorias()) + (1 * this.getEmpates());
	}
	
	public Integer getGolsPro() {
		Integer res = 0;
		for (Jogo jogo : jogos) {
			if (this.time.equals(jogo.getTimeCasa())) {
				res+= jogo.getPlacarTimeCasa();
				continue;
			}
			else {
				res+= jogo.getPlacarTimeVisitante();
				continue;
			}
		}
		return res;
	}
	
	public Integer getGolsSofridos() {
		Integer res = 0;
		for (Jogo jogo : jogos) {
			if (this.time.equals(jogo.getTimeCasa())) {
				res+= jogo.getPlacarTimeVisitante();
				continue;
			}
			else {
				res+= jogo.getPlacarTimeCasa();
				continue;
			}
		}
		return res;
	}
	
	public static String getCabecalho() {
		return "Time | Pontos | Jogos | Vitorias | Empates | Derrotas | GP | GC | SG";
	}
	
	public String getRepresentacaoClassificacao() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(time.getNome()).append(" | ");
		sb.append(getPontos()).append(" | ");
		sb.append(getQuantidadeJogos()).append(" | ");
		sb.append(getVitorias()).append(" | ");
		sb.append(getEmpates()).append(" | ");
		sb.append(getDerrotas()).append(" | ");
		sb.append(getGolsPro()).append(" | ");
		sb.append(getGolsSofridos()).append(" | ");
		sb.append(getSaldoGols());
		
		return sb.toString();
	}
	
	public Integer getSaldoGols() {
		return this.getGolsPro() - this.getGolsSofridos();
	}
	
	public Integer getQuantidadeJogos() {
		return this.jogos.size();
	}

	public Classificacao(Time time) {
		super();
		this.time = time;
		this.jogos = new ArrayList<Jogo>();
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public List<Jogo> getJogos() {
		return jogos;
	}

	public void setJogos(List<Jogo> jogos) {
		this.jogos = jogos;
	}

	@Override
	public int compareTo(Classificacao o) {
		Integer res = 0;
		
		res = this.getPontos().compareTo(o.getPontos());
		if (res == 0) {
			res = this.getVitorias().compareTo(o.getVitorias());
			if (res == 0) {
				res = this.getSaldoGols().compareTo(o.getSaldoGols());
				if (res == 0) {
					res = this.getGolsPro().compareTo(o.getGolsPro());
				}
			}
		}
		return res;
	}
	
	
}
