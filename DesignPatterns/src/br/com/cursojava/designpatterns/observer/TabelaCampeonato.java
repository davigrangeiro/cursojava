package br.com.cursojava.designpatterns.observer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class TabelaCampeonato implements Observer{
	
	private List<Rodada> rodadas;
	
	private List<Classificacao> classificacao;

	public TabelaCampeonato() {
		super();
		this.rodadas = new ArrayList<Rodada>();
		this.classificacao = new ArrayList<>();
	}

	public List<Rodada> getRodadas() {
		return rodadas;
	}

	public void preencherClassificacao() {
		for (Rodada rodada : rodadas) {
			for (Jogo jogo : rodada.getJogos()) {
				Classificacao casa = null, visitante = null ;
				
				for (Classificacao classificacao2 : classificacao) {
					if (jogo.getTimeCasa().equals(classificacao2.getTime())) {
						casa = classificacao2;
					} 
					if (jogo.getTimeVisitante().equals(classificacao2.getTime())) {
						visitante = classificacao2;
					}
				}
				
				
				if (casa == null && visitante != null) {
					visitante.getJogos().add(jogo);
				}
				
				if (visitante == null && casa != null) {
					casa.getJogos().add(jogo);
				}
				
				if (casa == null) {
					Classificacao c = new Classificacao(jogo.getTimeCasa());
					c.getJogos().add(jogo);
					classificacao.add(c);
				}
				if (visitante == null) {
					Classificacao c = new Classificacao(jogo.getTimeVisitante());
					c.getJogos().add(jogo);
					classificacao.add(c);
				}
			}
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		Collections.sort(classificacao);
		
		sb.append(Classificacao.getCabecalho()).append("\n");
		for (Classificacao atual : classificacao) {
			sb.append(atual.getRepresentacaoClassificacao()).append("\n");
		}
		return sb.toString();
	}

	@Override
	public void update(Observable o, Object arg) {
		preencherClassificacao();
		System.out.println(this.toString());
	}
	
	
	
	
	
}
