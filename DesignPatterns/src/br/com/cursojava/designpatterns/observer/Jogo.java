package br.com.cursojava.designpatterns.observer;

import java.util.Observable;
import java.util.Observer;

public class Jogo extends Observable{
	
	private Time timeCasa;
	private Time timeVisitante;
	
	private Integer placarTimeCasa;
	private Integer placarTimeVisitante;
	
	
	public void golTimeCasa() {
		placarTimeCasa += 1;
		super.setChanged();
		this.notifyObservers();
	}

	public void golTimeVisitante() {
		placarTimeVisitante += 1;
		super.setChanged();
		this.notifyObservers();
	}
	
	
	public void jogar() {
		Integer qGols = ((Double) (Math.random() * 11.0)).intValue();
		for (int i = 0; i < qGols; i++) {
			if (Math.random() > 0.5) {
				this.golTimeCasa();
			} else {
				this.golTimeVisitante();;
			}
		}
	}
	
	public Jogo(Time timeCasa, Time timeVisitante, Observer v) {
		super();
		this.timeCasa = timeCasa;
		this.timeVisitante = timeVisitante;
		this.placarTimeCasa = 0;
		this.placarTimeVisitante = 0;
		this.addObserver(v);
	}

	public Time getTimeCasa() {
		return timeCasa;
	}

	public void setTimeCasa(Time timeCasa) {
		this.timeCasa = timeCasa;
	}

	public Time getTimeVisitante() {
		return timeVisitante;
	}

	public void setTimeVisitante(Time timeVisitante) {
		this.timeVisitante = timeVisitante;
	}

	public Integer getPlacarTimeCasa() {
		return placarTimeCasa;
	}

	public void setPlacarTimeCasa(Integer placarTimeCasa) {
		this.placarTimeCasa = placarTimeCasa;
	}

	public Integer getPlacarTimeVisitante() {
		return placarTimeVisitante;
	}

	public void setPlacarTimeVisitante(Integer placarTimeVisitante) {
		this.placarTimeVisitante = placarTimeVisitante;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((placarTimeCasa == null) ? 0 : placarTimeCasa.hashCode());
		result = prime * result + ((placarTimeVisitante == null) ? 0 : placarTimeVisitante.hashCode());
		result = prime * result + ((timeCasa == null) ? 0 : timeCasa.hashCode());
		result = prime * result + ((timeVisitante == null) ? 0 : timeVisitante.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Jogo other = (Jogo) obj;
		if (placarTimeCasa == null) {
			if (other.placarTimeCasa != null)
				return false;
		} else if (!placarTimeCasa.equals(other.placarTimeCasa))
			return false;
		if (placarTimeVisitante == null) {
			if (other.placarTimeVisitante != null)
				return false;
		} else if (!placarTimeVisitante.equals(other.placarTimeVisitante))
			return false;
		if (timeCasa == null) {
			if (other.timeCasa != null)
				return false;
		} else if (!timeCasa.equals(other.timeCasa))
			return false;
		if (timeVisitante == null) {
			if (other.timeVisitante != null)
				return false;
		} else if (!timeVisitante.equals(other.timeVisitante))
			return false;
		return true;
	}

	
}
