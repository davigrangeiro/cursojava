package br.com.calculo;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BaskaraTest {
	
	public Baskara baskara;
	
	
	@Before
	public void setUp() throws Exception {
		baskara = new Baskara(0.f, 0.f, 0.f);
	}

	@After
	public void tearDown() throws Exception {
		baskara = null;
	}

	@Test
	public void testBaskaraEmpty() {
		assertTrue("Teste Valores Iniciais", 0.f == baskara.getA());
		assertTrue("Teste Valores Iniciais", 0.f == baskara.getB());
		assertTrue("Teste Valores Iniciais", 0.f == baskara.getC());
	}
	
	@Test(expected = RuntimeException.class)
	public void testDeltaNegativo() {
		baskara = new Baskara(2.f, 1f, 4.f);
		baskara.perform();
	}

	@Test
	public void testPerformXUnico() {
		baskara = new Baskara(1.f, 4f, 4.f);
		baskara.perform();
		assertTrue(baskara.getX1() == baskara.getX2());
	}
	
	@Test
	public void testPerform() {
		baskara = new Baskara(1.f, 4f, 3.f);
		baskara.perform();
		assertTrue(baskara.getX1() == -3f);
		assertTrue(baskara.getX2() == -1f);
	}

}
