package br.com.cursojava;

public class CalculadoraComplexaImpl implements CalculadoraComplexa {

	@Override
	public Integer pow(Integer base, Integer... numeros) {
		Integer res = null;
		if (null != base && null != numeros && numeros.length > 0) {
			res = base;
			
			for (int i = 0; i < numeros.length; i++) {
				for (int j = 0; j < numeros[i]; j++) {
					res *= res;
				}
			}
		}
		return res;
	}

	@Override
	public Integer factorial(Integer base) {
		Integer res = null;
		if (null != base) {
			res = base;
			
			for (int i = base-1; i > 0 ; i--) {
				res *= i;
			}
		}
		return res;
	}

	static Integer[] resFibs = new Integer[5000]; 
	
	@Override
	public Integer fib(Integer pos) {
		if (resFibs[pos] != null) {
			return resFibs[pos];
		}
		if (pos == 2 || pos == 1) return 1;
		Integer res = fib(pos-1) + fib(pos-2);
		resFibs[pos] = res;
		return res;
	}

}
