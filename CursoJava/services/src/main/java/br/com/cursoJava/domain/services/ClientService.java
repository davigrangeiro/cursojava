package br.com.cursoJava.domain.services;

import java.io.Serializable;
import java.util.List;

import br.com.cursoJava.domain.Client;
import br.com.cursoJava.domain.exceptions.ClientException;
import br.com.cursoJava.domain.services.impl.cliente.filtro.ClientSearchFilter;

public interface ClientService extends Serializable{

	public void insertClient(Client client) throws ClientException;
	
	public void deleteClient(Client client);
	
	public void updateClient(Client client) throws ClientException;
	
	public List<Client> listClients();
	
	public List<Client> searchClients(ClientSearchFilter filter);
	
}
