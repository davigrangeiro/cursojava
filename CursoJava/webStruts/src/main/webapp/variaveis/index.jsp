<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
	<head>
		<script type="text/javascript" src="<c:url value='js/testeVariaveis.js' />">
		</script>
	</head>
	
	<body>
		<h2>Este � um teste de vari�veis de request e session.</h2>
		<br/>
		<form action="testeVariaveis.do?mode="  method="post" name="testeVariaveisForm">
			<input type="button" value="Send Request" onclick="submitForm('requestTest')">
			<br/>
			<input type="button" value="Send Session" onclick="submitForm('sessionTest')">
		</form>	
	</body>
</html>