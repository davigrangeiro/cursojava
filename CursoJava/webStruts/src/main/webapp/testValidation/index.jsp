<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<html>
	<head>
	</head>
	
	<body>
	
		<logic:messagesPresent message="true">
			<ul>
				<html:messages id="message" message="true" >
					<li  style="color:green;"><bean:write name="message"/> </li>
				</html:messages>			
			</ul>
		</logic:messagesPresent>
		
		
		<logic:messagesPresent>
			<ul>
				<html:messages id="message">
					<li style="color:red;"><bean:write name="message"/> </li>
				</html:messages>			
			</ul>
		</logic:messagesPresent>
		
	
		<h2>Este � um teste de valida��o dos conte�dos do formul�rio Struts</h2>
		<br/>
		<form action="testValidation.do"  method="post" name="validationExampleForm">
			<input type="text" name="content"/>
			<input type="submit" value="Send" />
		</form>	
	</body>
</html>