package br.com.cursojava.struts.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class ValidateExampleForm extends ActionForm {

	private static final String EMPTY_STRING = "";

	private static final long serialVersionUID = -5824800940901778242L;
	
	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.content = EMPTY_STRING;
	}

	@Override
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if (null == content || EMPTY_STRING.equals(content)) {
			errors.add("commom.error", new ActionMessage("validateExample.content.empty"));
		}
		
		return errors;
	}

}
