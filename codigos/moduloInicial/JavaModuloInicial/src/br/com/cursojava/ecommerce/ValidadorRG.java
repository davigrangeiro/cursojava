package br.com.cursojava.ecommerce;

import br.com.cursojava.ecommerce.exception.DocumentoInvalidoException;

public class ValidadorRG extends ValidadorDocumento {

	@Override
	public void validarDocumento(String documento) throws DocumentoInvalidoException {
		if (documento == null) {
			throw new DocumentoInvalidoException();
		}
	}

}
