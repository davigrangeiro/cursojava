package br.com.cursojava.ecommerce;

import br.com.cursojava.ecommerce.apoio.TipoDocumentoEnum;
import br.com.cursojava.ecommerce.exception.DocumentoInvalidoException;

public abstract class ValidadorDocumento {

	public abstract void validarDocumento(String documento) throws DocumentoInvalidoException;
	
	public static ValidadorDocumento getInstance(TipoDocumentoEnum tipoDocumento) {
		ValidadorDocumento res = null;
		
		switch (tipoDocumento) {
		case RG:
			res = new ValidadorRG();
			break;
		case CPF:
			res = new ValidadorCPF();
			break;
		default:
			break;
		}
		
		return res;
	}
	
	
	public static void main(String[] args) {
		try {
			ValidadorDocumento.getInstance(TipoDocumentoEnum.RG).validarDocumento("440786605");
		} catch (DocumentoInvalidoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
