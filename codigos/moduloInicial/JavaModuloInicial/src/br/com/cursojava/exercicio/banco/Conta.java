package br.com.cursojava.exercicio.banco;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.cursojava.exercicio.banco.exceptions.ContaBloqueadaException;
import br.com.cursojava.exercicio.banco.exceptions.SaldoInsulficienteException;

public abstract class Conta {

	private Integer numeroConta;
	private Integer numeroDigito;
	private Boolean bloqueio;
	
	public List lancamentos;
	
	public Conta() {
		super();
		this.lancamentos = new ArrayList<>();
		this.bloqueio = false;
	}
	
	public Conta(Integer numeroConta, Integer numeroDigito, Boolean bloqueio) {
		super();
		this.numeroConta = numeroConta;
		this.numeroDigito = numeroDigito;
		this.bloqueio = bloqueio;
		this.lancamentos = new ArrayList<>();
	}

	public abstract BigDecimal getTaxaRendimento();
	
	public abstract BigDecimal getIR();
	
	public abstract void validarLancamento(Lancamento lancamento) throws SaldoInsulficienteException;
	
	public void realizarLancamento(BigDecimal valor, Date dataLancamento) throws ContaBloqueadaException, SaldoInsulficienteException {
		Lancamento lancamento = new Lancamento(dataLancamento, valor);
		this.verificarBloqueioConta();
		this.validarLancamento(lancamento);
		this.lancamentos.add(lancamento);
		System.out.println(getSaldo());
	}
	
	private void verificarBloqueioConta() throws ContaBloqueadaException {
		if (this.bloqueio) {
			throw new ContaBloqueadaException("A conta esta bloqueada");
		}
	}
	
	
	/**
	 * Saldo a partir de um lancamento
	 * lancamento * (1 + taxaRendimento ^ (diferenca Datas))
	 * decrescido do IR
	 * 
	 * @return saldo atual
	 */
	public BigDecimal getSaldo() {
		BigDecimal res = BigDecimal.ZERO;
		
		for (Object object : lancamentos) {
			Lancamento lancamento = (Lancamento) object;
			res = res.add(lancamento.getValorAtualizado(this.getTaxaRendimento(), this.getIR()));
		}
		
		return res;
	}

	public Integer getNumeroConta() {
		return numeroConta;
	}

	public void setNumeroConta(Integer numeroConta) {
		this.numeroConta = numeroConta;
	}

	public Integer getNumeroDigito() {
		return numeroDigito;
	}

	public void setNumeroDigito(Integer numeroDigito) {
		this.numeroDigito = numeroDigito;
	}

	public List getLancamentos() {
		return lancamentos;
	}

	public void setLancamentos(List lancamentos) {
		this.lancamentos = lancamentos;
	}

	public Boolean getBloqueio() {
		return bloqueio;
	}

	public void setBloqueio(Boolean bloqueio) {
		this.bloqueio = bloqueio;
	}
	
	
	
	
}
