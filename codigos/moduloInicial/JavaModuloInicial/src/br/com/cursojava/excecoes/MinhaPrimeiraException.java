package br.com.cursojava.excecoes;

public class MinhaPrimeiraException extends Exception{

	private String message;

	public MinhaPrimeiraException(String message) {
		super();
		this.message = message;
	}

	public MinhaPrimeiraException() {
		super();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}
