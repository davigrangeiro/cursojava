package br.com.cursojava.comentarios;

/*
 * Comentário de bloco em java
 * Pode conter mais de uma linha e ser posicionado em qualquer ponto do código.
 */

/**
 * JAVADOC - Documentação das classes Java.
 * Tem o objetivo de descrever o propósito dos
 * componentes da solução. 
 * 
 * @author Davi Grangeiro (Anotação de autor da classe)
 *
 */
public class ExemploComentarios {

	/**
	 * Pode ser utilizado em atributos
	 */
	private Integer valorInteiro;
	
	/**
	 * Deve ser utilizado, ferramentas verificadoras de código reclamam isso.
	 */
	private Float   valorDecimal;
	
	/**
	 * Felizmente, há plugins que fazem isso pela gente. (JAUTODOC)
	 */
	private String  cadeiaDeCaracteres;

	
	/*
	 * Comentário de bloco em java
	 * Pode conter mais de uma linha e ser posicionado em qualquer ponto do código.
	 */
	

	public ExemploComentarios() {
		super();
		// Comentário de linha, pode ser utilizado em qualquer ponto do código.
	}
	

	/**
	 * Métodos devem ter comentário também, notem que para cada parâmetro há uma anotação a fim de descrever seu propósito.
	 * @param valorInteiro
	 * @param valorDecimal
	 * @param cadeiaDeCaracteres 
	 */
	public ExemploComentarios(Integer valorInteiro, Float valorDecimal, String cadeiaDeCaracteres) {
		super();
		this.valorInteiro = valorInteiro;
		this.valorDecimal = valorDecimal;
		this.cadeiaDeCaracteres = cadeiaDeCaracteres;

		/*
		 * Comentário de bloco em java
		 * Pode conter mais de uma linha e ser posicionado em qualquer ponto do código.
		 */
	}

	
	public Integer getValorInteiro() {
		return valorInteiro;
	}


	public void setValorInteiro(Integer valorInteiro) {
		this.valorInteiro = valorInteiro;
	}


	public Float getValorDecimal() {
		return valorDecimal;
	}


	public void setValorDecimal(Float valorDecimal) {
		this.valorDecimal = valorDecimal;
	}


	public String getCadeiaDeCaracteres() {
		return cadeiaDeCaracteres;
	}


	public void setCadeiaDeCaracteres(String cadeiaDeCaracteres) {
		this.cadeiaDeCaracteres = cadeiaDeCaracteres;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cadeiaDeCaracteres == null) ? 0 : cadeiaDeCaracteres.hashCode());
		result = prime * result + ((valorDecimal == null) ? 0 : valorDecimal.hashCode());
		result = prime * result + ((valorInteiro == null) ? 0 : valorInteiro.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExemploComentarios other = (ExemploComentarios) obj;
		if (cadeiaDeCaracteres == null) {
			if (other.cadeiaDeCaracteres != null)
				return false;
		} else if (!cadeiaDeCaracteres.equals(other.cadeiaDeCaracteres))
			return false;
		if (valorDecimal == null) {
			if (other.valorDecimal != null)
				return false;
		} else if (!valorDecimal.equals(other.valorDecimal))
			return false;
		if (valorInteiro == null) {
			if (other.valorInteiro != null)
				return false;
		} else if (!valorInteiro.equals(other.valorInteiro))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "ExemploComentarios [valorInteiro=" + valorInteiro + ", valorDecimal=" + valorDecimal
				+ ", cadeiaDeCaracteres=" + cadeiaDeCaracteres + "]";
	}


	
	
}
