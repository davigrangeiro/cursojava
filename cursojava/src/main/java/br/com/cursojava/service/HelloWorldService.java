package br.com.cursojava.service;

import javax.ejb.Stateless;

@Stateless
public class HelloWorldService {

	public void hello(String name) {
		System.out.println("Hello, " + name);
	}
	
}