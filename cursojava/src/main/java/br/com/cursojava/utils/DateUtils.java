package br.com.cursojava.utils;

import java.util.Date;

public class DateUtils {

	public static Long daysBetween(Date later, Date earlier) {
		Long res = earlier.getTime() - later.getTime();
		
		return res / (1000*60*60*24);
	}
	
	public static Long yearsBetween(Date later, Date earlier) {
		return daysBetween(later, earlier) / 365;
	}
	
	
}
