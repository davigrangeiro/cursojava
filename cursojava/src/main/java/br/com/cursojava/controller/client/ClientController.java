package br.com.cursojava.controller.client;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.com.cursojava.controller.GenericController;
import br.com.cursojava.domain.Client;
import br.com.cursojava.domain.exceptions.ClientException;
import br.com.cursojava.domain.services.ClientService;
import br.com.cursojava.domain.services.impl.client.filter.ClientSearchFilter;

@Named
@SessionScoped
public class ClientController extends GenericController implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Client> clients;
	
	private boolean insert;

	private boolean update;
	
	private Client client;

	private Integer clientId;
	
	private ClientSearchFilter searchFilter;
	
	@EJB
	private ClientService clientService;
	
	@PostConstruct
	public void initialize(){
		this.client = new Client();
		this.searchFilter = new ClientSearchFilter();
		closeClientInsert();
		closeClientUpdate();
		listClients();
	}

	private void closeClientUpdate() {
		this.update = false;
	}

	private void closeClientInsert() {
		this.insert = false;
	}

	private void listClients() {
		this.clients = clientService.listClients();
	}
	
	public void deleteClient() {
		Client client = new Client();
		client.setId(this.clientId);
		this.clientService.deleteClient(client);
		super.addSuccessMessage("Cliente Excluído", this.clientId + "");
		this.clientId= null;
		this.listClients();
	}
	
	public void updateClient() {
		if (validate()) {
			try {
				this.clientService.updateClient(getClient());
				super.addSuccessMessage("Cliente Alterado", this.client.getId() + "");
				closeClientUpdate();
				this.client = new Client();
				this.listClients();
			} catch (ClientException e) {
				super.addErrorMessage("Ocorreram erros ao alterar o cliente: ", e.getMessage());
			}
		}
	}
	
	public void requestClientUpdate() {
		this.update = true;
		closeClientInsert();
	}
	
	public void searchClient() {
		this.clients = this.clientService.searchClients(searchFilter);
	}
	
	public void requestClientInsert(){
		this.insert = true;
	}
	
	public void registerClient() {
		if (validate()) {
			try {
				this.clientService.insertClient(this.client);
				super.addSuccessMessage("Cliente Cadastrado", this.client.toString());
				this.client = new Client();
				listClients();
				closeClientInsert();
			} catch (ClientException e) {
				super.addErrorMessage("Ocorreram erros ao cadastrar o cliente: ", e.getMessage());
			}
		}
	}
	
	private boolean validate() {
		boolean res = true;
		
		if (null == client.getName() || client.getName().isEmpty()) {
			super.addErrorMessage("Dados inválidos", "O nome do cliente não deve ser vazio");
			res = false;
		}

		if (null == client.getDocumentNumber() || client.getDocumentNumber().isEmpty()) {
			super.addErrorMessage("Dados inválidos", "O CPF do cliente não deve ser vazio");
		
			res = false;
		}

		if (null == client.getBirthdayDate()) {
			super.addErrorMessage("Dados inválidos", "A idade do cliente não deve ser vazia");
			res = false;
		}

		if (null == client.getGender() || client.getGender().isEmpty()) {
			super.addErrorMessage("Dados inválidos", "O gênero do cliente não deve ser vazio");
			res = false;
		}
		
		return res;
	}

	public List<Client> getClients() {
		return clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public boolean isInsert() {
		return insert;
	}

	public void setInsert(boolean insert) {
		this.insert = insert;
	}

	public boolean isUpdate() {
		return update;
	}

	public void setUpdate(boolean update) {
		this.update = update;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public ClientSearchFilter getSearchFilter() {
		return searchFilter;
	}

	public void setSearchFilter(ClientSearchFilter searchFilter) {
		this.searchFilter = searchFilter;
	}
	
}
