package br.com.cursojava.controller;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import br.com.cursojava.service.HelloWorldService;

@Named
@RequestScoped
public class HelloWorldController {

	@EJB
	private HelloWorldService helloWorldService;
	
	private String name;

	public String hello() {
		this.helloWorldService.hello(this.name);
		return "";
	}
	
	public HelloWorldService getHelloWorldService() {
		return helloWorldService;
	}

	public void setHelloWorldService(HelloWorldService helloWorldService) {
		this.helloWorldService = helloWorldService;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
}
