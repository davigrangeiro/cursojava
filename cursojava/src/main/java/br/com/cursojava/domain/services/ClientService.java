package br.com.cursojava.domain.services;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import br.com.cursojava.domain.Client;
import br.com.cursojava.domain.exceptions.ClientException;
import br.com.cursojava.domain.services.impl.client.filter.ClientSearchFilter;

@Local
public interface ClientService extends Serializable{

	public void insertClient(Client client) throws ClientException;
	
	public void deleteClient(Client client);
	
	public void updateClient(Client client) throws ClientException;
	
	public List<Client> listClients();
	
	public List<Client> searchClients(ClientSearchFilter filter);
	
}
