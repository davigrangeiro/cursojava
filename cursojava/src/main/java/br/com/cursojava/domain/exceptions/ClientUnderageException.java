package br.com.cursojava.domain.exceptions;

public class ClientUnderageException extends ClientException {

	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {
		return "O cliente deve ser maior de idade.";
	}

}
