package br.com.cursojava.domain.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.cursojava.domain.Client;

@Stateless
public class ClientRepository {

	@PersistenceContext(name="cursojava")
	private EntityManager em;

	public void save(Client client) {
		em.persist(client);
	}
	
	public void update(Client client) {
		em.merge(client);
	}

	public Long countClientByDocumentNumber(String documentNumber) {
		Long res = 0l;
		TypedQuery<Long> q = em.createQuery("select count(c) from Client c where c.documentNumber = " + documentNumber, Long.class);
		res = q.getSingleResult();
		return res;
	}

	public List<Client> findAll() {
		List<Client> res = null;
		TypedQuery<Client> q = em.createQuery("select c from Client c", Client.class);
		res = q.getResultList();
		return res;
	}

	public void delete(Client cliente) {
		em.remove(em.find(Client.class, cliente.getId()));
	}

	public List<Client> findByNameContaining(String name) {
		List<Client> res = null;
		TypedQuery<Client> q = em.createQuery("select c from Client c where c.name like '%"+name+"%'", Client.class);
		res = q.getResultList();
		return res;
	}
	
}
