package br.com.cursojava.domain.exceptions;

public class ClientAlreadyExistsException extends ClientException {

	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {
		return "O cliente informado já existe na base de dados";
	}
	
}
