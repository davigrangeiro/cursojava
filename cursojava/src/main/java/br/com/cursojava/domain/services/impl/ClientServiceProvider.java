package br.com.cursojava.domain.services.impl;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.com.cursojava.domain.Client;
import br.com.cursojava.domain.exceptions.ClientAlreadyExistsException;
import br.com.cursojava.domain.exceptions.ClientException;
import br.com.cursojava.domain.exceptions.ClientUnderageException;
import br.com.cursojava.domain.exceptions.InvalidDocumentNumberException;
import br.com.cursojava.domain.repository.ClientRepository;
import br.com.cursojava.domain.services.ClientService;
import br.com.cursojava.domain.services.impl.client.filter.ClientSearchFilter;
import br.com.cursojava.utils.DateUtils;

@Stateless
public class ClientServiceProvider implements ClientService {

	private static final long serialVersionUID = -1L;
	
	@EJB
	private ClientRepository clientRepository;
	
	public void insertClient(Client client) throws ClientException {
		this.validateCliente(client, false);
		this.clientRepository.save(client);
	}

	private void clientAlreadyExists(Client client) throws ClientException {
		boolean found = this.clientRepository.countClientByDocumentNumber(client.getDocumentNumber()) > 0;
		if (found) {
			throw new ClientAlreadyExistsException();
		}
	}

	private void validateCliente(Client client,boolean isUpdate) throws ClientException{
		if (DateUtils.yearsBetween(client.getBirthdayDate(), new Date()).compareTo(18l) < 0) {
			throw new ClientUnderageException();
		}
		if ("11111111111".equals(client.getDocumentNumber())) {
			throw new InvalidDocumentNumberException();
		}
		if (!isUpdate) {
			this.clientAlreadyExists(client);
		}
		
	}

	public void deleteClient(Client cliente) {
		this.clientRepository.delete(cliente);
	}

	public void updateClient(Client client) throws ClientException {
		validateCliente(client, true);
		this.clientRepository.update(client);
	}

	public List<Client> listClients() {
		return this.clientRepository.findAll();
	}

	public List<Client> searchClients(ClientSearchFilter filter) {
		List<Client> res = this.clientRepository.findByNameContaining(filter.getName());
		return res;
	}

	public ClientRepository getClientRepository() {
		return clientRepository;
	}

	public void setClientRepository(ClientRepository clientRepository) {
		this.clientRepository = clientRepository;
	}

}
